# My base config

## Description

To keep my prefeered vim and bash settings available for quick setup of new workstation

For vim-ai plugin openAI API key is needed

```
echo "YOUR_OPENAI_API_KEY" > ~/.config/openai.token
```
### local models using ollama

Install ollama

```bash
snap install ollama

ollama pull deepseek-r1

ollama run deepseek-r1
```

localhost:11434 is the openai endpoint

see vimrc-ollama settings

edit and complete does not work well with a reasonining model and it will use the
 chat mode and add /think and other non code stuff


## Instructions

```
git clone git@gitlab.com:PeterHedman/base-config.git
cp base-config/bashrc  ~/.bashrc
```

## vim plugins

### Automatic

```
./setup-vim.sh
```

### Manual
Install pathogen plugin manager

```
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

#### vim rc plugins to clone

```
git clone https://github.com/lepture/vim-jinja.git ~/.vim/bundle/vim-jinja
git clone https://github.com/rust-lang/rust.vim ~/.vim/bundle/rust
git clone https://github.com/da-x/vader.vim ~/.vim/bundle/vader
git clone https://github.com/saltstack/salt-vim.git ~/.vim/bundle/salt-vim
git clone https://github.com/fatih/vim-go.git ~/.vim/bundle/vim-go
git clone  "https://github.com/junegunn/fzf.vim" ~/.vim/bundle/fzf
git clone  "https://github.com/itchyny/lightline.vim" ~/.vim/bundle/lightline
git clone  "https://github.com/scrooloose/nerdtree" ~/.vim/bundle/nerdtree
git clone https://github.com/dbeniamine/cheat.sh-vim.git  ~/.vim/bundle/nerdtree
```

## Good to know commands

```
<F8> yanks current line to excute as command in shell
<C-p> opens browser for markdown preview github style
:NERDTree "to browse current file directory"
Gstatus G followed by git command for git wrapping
:IndentGuidesEnable
:IndentGuidesDisable
:IndentGuidesToggle
:Cheat <query>
K or \KK to query https://cht.sh directly from current word selection or line
```

## Install languages

### GO

```
wget https://dl.google.com/go/go1.15.8.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.15.8.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
mkdir $HOME/go
```

### RUST

```
curl https://sh.rustup.rs -sSf | sh
```

### Pyhton3

```
sudo apt-get update
sudo apt-get install python3.6
```

### C

```
sudo apt install gcc
```

### Language Cheat sheets

```
curl https://cheat.sh/rsync
https://upsuper.github.io/rust-cheatsheet/
https://devhints.io/go
https://perso.limsi.fr/pointal/_media/python:cours:mementopython3-english.pdf
https://devhints.io/bash
https://devhints.io/makefile
```

### Build linux kernel

#### Debian

```
sudo apt-get install libncurses5-dev gcc make git exuberant-ctags bc libssl-dev
```

#### Centos

```
sudo yum install gcc make git ctags ncurses-devel openssl-devel
```

### Common

```
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
cd linux-stable
git checkout -b stable tag
make defconfig
or
make menuconfig
make -j
```

#### Install it

```
sudo update-grub2
```

