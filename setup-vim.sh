#!/bin/bash -eu

stat $HOME/.vimrc

if [ ! "$(which vim)" ]
then
  echo "vim needs to be installed for this script to work"
  exit 1;
fi

if [ ! "$(which curl)" ]
then
  echo "curl is required for this script to work"
  exit 1;
fi

if [ ! -f "$HOME/.vimrc" ]
then
  cp vimrc $HOME/.vimrc
elif [ "$(diff -q vimrc $HOME/.vimrc)" != "" ]
then
  cp $HOME/.vimrc $HOME/.vimrc-backup
  echo "previous vimrc copied to $HOME/.vimrc-backup"
  cp vimrc $HOME/.vimrc
fi

PLUGINS=(
  "https://github.com/lepture/vim-jinja.git"
  "https://github.com/rust-lang/rust.vim"
  "https://github.com/da-x/vader.vim"
  "https://github.com/saltstack/salt-vim.git"
  "https://github.com/fatih/vim-go.git"
  "https://github.com/junegunn/fzf.vim"
  "https://github.com/itchyny/lightline.vim"
  "https://github.com/scrooloose/nerdtree"
  "https://github.com/tpope/vim-fugitive"
  "https://github.com/nathanaelkane/vim-indent-guides.git"
  "https://github.com/tpope/vim-surround"
  "https://github.com/dbeniamine/cheat.sh-vim.git"
  "https://github.com/madox2/vim-ai.git"
)

for plugin in "${PLUGINS[@]}"
do
  BUNDLE_DIR=$(basename $plugin | cut -f1 -d".")
  if [ ! -d $HOME/.vim/bundle/$BUNDLE_DIR ]
  then
    git clone $plugin $HOME/.vim/pack/plugins/start/$BUNDLE_DIR
  fi
done

echo "vimrc and plugins installed"
