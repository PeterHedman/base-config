export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33'
EDITOR='vim'
HISTFILESIZE=2000
HISTSIZE=2000
HISTTIMEFORMAT="%Y-%m-%d %H:%M.%S | "
PS1='\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[00m\]\w\[\033[00m\]\$'
PS4='$0 line $LINENO: '
EDITOR='vim'
VISUAL='vim'
PATH=$PATH":/home/peter/scripts"
PATH=$PATH:/usr/local/go/bin
# shell options
set -o emacs
set +o vim
# bash shell options
shopt -s cmdhist
shopt -s lithist
function lsd
{
	date=$1
	ls -l | grep -i "^.\{36\}$date" | cut -c50-
}

# function to set terminal title  
function set-title() {
  if [[ -z "$ORIG" ]]; then
    ORIG=$PS1
  fi
  TITLE="\[\e]2;$*\a\]"
  PS1=${ORIG}${TITLE}
}
